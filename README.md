# TheLoginTestSuite

The login tests for the https://the-internet.herokuapp.com/login

### Install RobotFramework and selenium library
```
pip install robotframework
pip install robotframework-seleniumlibrary

```

### Install ChromeDriver
[download chromedriver](http://chromedriver.storage.googleapis.com/index.html)
and move chromedriver to your local user bin (/usr/bin)

### Run Tests
```
robot TestCases/

```

### Run Tests with Tags
```
robot -i Regression TestCases/

```

### Run Assigned Test
```
robot -t "User Should Be Able to Logout" TestCases/

```

### Tests Report
report.html
log.html